# Configuration file for neovim

The configuration file should be placed in `~/.config/nvim`.

``` bash
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
mkdir -p ~/.config/nvim
ln init.vim ~/.config/nvim/
```

Get the status of the plugins in neovim with `:PlugStatus`.
Install the missing plugins with `:PlugInstall module`

For the installation of Deoplete, see the [github page of the project](https://github.com/Shougo/deoplete.nvim).
