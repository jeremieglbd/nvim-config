call plug#begin('~/.local/share/nvim/plugged')

Plug 'Shougo/deoplete.nvim'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdTree'
Plug 'bling/vim-airline'

call plug#end()

inoremap <expr> <C-j> pumvisible() ? "\<C-n>" : "\<C-j>"
inoremap <expr> <C-k> pumvisible() ? "\<C-p>" : "\<C-k>"
nmap <C-n> :NERDTreeToggle<CR>
set rnu
set nu
set shiftwidth=2
set autoindent
set smartindent
